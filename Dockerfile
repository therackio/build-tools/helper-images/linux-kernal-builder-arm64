ARG PARENT_IMAGE
FROM $PARENT_IMAGE

LABEL maintainer="gitlab@therack.io"
LABEL description="Basic Ubuntu-based Linux kernal builder image for ARM64 SBCs"

ENV DEBIAN_FRONTEND=noninteractive
ENV TERM=xterm
ENV LC_ALL=C.UTF-8
ENV LANGUAGE=C.UTF-8
ENV LANG=C.UTF-8

RUN \
    set -eux && \
    apt -qq update --fix-missing && \
    apt -qq -y install git build-essential libncurses-dev bison flex libssl-dev libelf-dev bc && \
    apt -qq -y -o Dpkg::Options::="--force-confold" full-upgrade && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
